﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GASServer.Models
{
    [Table("DotaGames")]
    public class DotaGame
    {
        public enum DotaGameResult
        {
            Undefined = 0,
            DireWinner = 1,
            RadiantWinner = 2,
            Draw = 3
        }

        [Column("id"), Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public ulong Id { get; set; }

        [Column("game_client_id")] public string GameClientId { get; set; }

        [InverseProperty("Game")] public List<DotaGamePlayer> GamePlayers { get; set; }

        [Column("result")]
        public string ResultAsString
        {
            get { return Result.ToString(); }
            set
            {
                Result = Enum.TryParse(value, out DotaGameResult result)
                    ? result
                    : DotaGameResult.Undefined;
            }
        }

        [NotMapped] public DotaGameResult Result { get; set; }
    }
}