﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GASServer.Models
{
    [Table("DotaGamePlayers")]
    public class DotaGamePlayer
    {
        public enum DotaTeamType
        {
            Radiant,
            Dire
        }

        [Column("dota_game_id"), Key] public ulong DotaGameId { get; set; }

        [Column("player_id"), Key] public ulong PlayerId { get; set; }

        [Column("is_capitan"), Required] public bool IsCaptain { get; set; }

        [Column("team_type"), Required] public DotaTeamType TeamType { get; set; }

        [Column("pick_order"), Required] public uint PickOrder { get; set; }

        [Required] public DotaGame Game { get; set; }

        [Required] public Player Player { get; set; }
    }
}