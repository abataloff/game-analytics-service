﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace GASServer.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<DotaGame> DotaGames { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<DotaGamePlayer> DotaGamePlayers { get; set; }

        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#if DEBUG
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseLoggerFactory(new LoggerFactory().AddConsole());
#endif
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DotaGamePlayer>()
                .HasKey(c => new {c.DotaGameId, c.PlayerId});
        }
    }
}