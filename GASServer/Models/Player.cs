﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace GASServer.Models
{
    [Table("Players")]
    public class Player
    {
        [Column("id"), Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public ulong Id { get; set; }

        [Column("player_client_id")] public string PlayerClientId { get; set; }

        [Column("nickname")] public string Nickname { get; set; }

        [InverseProperty("Player")] public List<DotaGamePlayer> DotaGame { get; set; }
    }
}