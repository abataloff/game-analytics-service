﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using GASServer.Models;
using Microsoft.AspNetCore.Mvc;
using GASServer.Controllers.v0.Dto;
using Microsoft.EntityFrameworkCore;

namespace GASServer.Controllers.v0
{
    [Route("dotagames")]
    [Route("v0/dotagames")]
    public class DotaGamesController : Controller
    {
        private const string AppKey = "UIR-FS8-KJE";

        private readonly ApplicationDbContext _context;

        public DotaGamesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<DotaGameDto> GetGames(string gameClientId = null)
        {
            var retVal = new List<DotaGameDto>();

            Request.Headers.TryGetValue(HttpRequestHeader.Authorization.ToString(), out var appKey);
            if (appKey.Count == 0 || appKey[0] != AppKey)
            {
                Response.StatusCode = (int) HttpStatusCode.Forbidden;
            }
            else
            {
                var dotaGames = _context.DotaGames.Include(g => g.GamePlayers).ThenInclude(dgp => dgp.Player)
                    .AsQueryable();
                if (gameClientId != null)
                {
                    dotaGames = dotaGames.Where(g => g.GameClientId == gameClientId);
                }

                foreach (var dotaGame in dotaGames)
                {
                    retVal.Add(DotaGameDto.FromModel(dotaGame));
                }
            }

            return retVal.AsEnumerable();
        }

        [HttpGet("{id}")]
        public DotaGameDto Get(ulong id)
        {
            var retVal = default(DotaGameDto);

            Request.Headers.TryGetValue(HttpRequestHeader.Authorization.ToString(), out var appKey);
            if (appKey.Count == 0 || appKey[0] != AppKey)
            {
                Response.StatusCode = (int) HttpStatusCode.Forbidden;
            }
            else
            {
                var dotaGame = _context.DotaGames.Include(g => g.GamePlayers)
                    .ThenInclude(dgp => dgp.Player).FirstOrDefault(g => g.Id == id);
                if (dotaGame != default(DotaGame))
                {
                    retVal = DotaGameDto.FromModel(dotaGame);
                }
                else
                {
                    Response.StatusCode = (int) HttpStatusCode.NotFound;
                }
            }

            return retVal;
        }

        [HttpPost]
        public DotaGameDto Post([FromBody] DotaGameDto dotaGameDto)
        {
            var retVal = default(DotaGameDto);

            Request.Headers.TryGetValue(HttpRequestHeader.Authorization.ToString(), out var appKey);
            if (appKey.Count == 0 || appKey[0] != AppKey)
            {
                Response.StatusCode = (int) HttpStatusCode.Forbidden;
            }
            else
            {
                var dotaGameModel = new DotaGame
                {
                    GameClientId = dotaGameDto.GameClientId,
                    ResultAsString = dotaGameDto.Result,
                    GamePlayers = new List<DotaGamePlayer>()
                };

                AddPlayersToGame(_context, dotaGameDto.DireTeam, DotaGamePlayer.DotaTeamType.Dire, dotaGameModel);
                AddPlayersToGame(_context, dotaGameDto.RadiantTeam, DotaGamePlayer.DotaTeamType.Radiant, dotaGameModel);

                _context.Add(dotaGameModel);
                _context.SaveChanges();

                retVal = DotaGameDto.FromModel(dotaGameModel);
            }

            return retVal;
        }

        private static void AddPlayersToGame(ApplicationDbContext context, string team,
            DotaGamePlayer.DotaTeamType teamType, DotaGame gameModel)
        {
            // Первым в массиве идет капитан
            var isCaptain = true;
            foreach (var playerId in team.Split(","))
            {
                var player = context.Players.Where(pl => pl.PlayerClientId == playerId).FirstOrDefault();
                if (player == default(Player))
                {
                    player = new Player
                    {
                        PlayerClientId = playerId,
                        Nickname = playerId
                    };
                    context.Add(player);
                }

                var dotaGamePlayer = new DotaGamePlayer
                {
                    Player = player,
                    Game = gameModel,
                    TeamType = teamType,
                    IsCaptain = isCaptain,
                    PickOrder = 0
                };
                gameModel.GamePlayers.Add(dotaGamePlayer);
                context.Add(dotaGamePlayer);
                isCaptain = false;
            }
        }
    }
}