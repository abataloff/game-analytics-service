﻿using System;
using System.Collections.Generic;
using GASServer.Models;
using Newtonsoft.Json;

namespace GASServer.Controllers.v0.Dto
{
    public class DotaGameDto
    {
        /// <summary>
        /// ID игры в GAS.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public ulong Id { get; set; }

        /// <summary>
        /// ID игры у клиента.
        /// </summary>
        [JsonProperty(PropertyName = "gameClientId")]
        public string GameClientId { get; set; }

        /// <summary>
        /// Список ID участников команды тьмы (разделитель - запятая). Самый первый в списке идет капитан.
        /// </summary>
        [JsonProperty(PropertyName = "direTeam")]
        public string DireTeam { get; set; }

        /// <summary>
        /// Список ID участников команды света (разделитель - запятая). Самый первый в списке идет капитан.
        /// </summary>
        [JsonProperty(PropertyName = "radiantTeam")]
        public string RadiantTeam { get; set; }

        /// <summary>
        /// Исход игры. Ограничений варианты значений нет.
        /// </summary>
        [JsonProperty(PropertyName = "result")]
        public string Result { get; set; }

        public static DotaGameDto FromModel(DotaGame dotaGame)
        {
            var retVal = default(DotaGameDto);

            if (dotaGame != null)
            {
                var direTeam = new List<string>();
                var radiantTeam = new List<string>();
                foreach (var dotaGamePlayer in dotaGame.GamePlayers)
                {
                    switch (dotaGamePlayer.TeamType)
                    {
                        case DotaGamePlayer.DotaTeamType.Dire:
                            direTeam.Add(dotaGamePlayer.Player.PlayerClientId);
                            break;
                        case DotaGamePlayer.DotaTeamType.Radiant:
                            radiantTeam.Add(dotaGamePlayer.Player.PlayerClientId);
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }

                retVal = new DotaGameDto
                {
                    Id = dotaGame.Id,
                    GameClientId = dotaGame.GameClientId,
                    DireTeam = string.Join(",", direTeam),
                    RadiantTeam = string.Join(",", radiantTeam),
                    Result = dotaGame.Result.ToString()
                };
            }

            return retVal;
        }
    }
}