﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using GASServer.Controllers.v1.Dto;
using GASServer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GASServer.Controllers.v1
{
    [Route("v1/dotagames")]
    public class DotaGamesController : Controller
    {
        const string AppKey = "UIR-FS8-KJE";

        private readonly ApplicationDbContext _context;

        public DotaGamesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<DotaGameDto> GetGames(string gameClientId = null)
        {
            var retVal = new List<DotaGameDto>();

            Request.Headers.TryGetValue(HttpRequestHeader.Authorization.ToString(), out var appKey);
            if (appKey.Count == 0 || appKey[0] != AppKey)
            {
                Response.StatusCode = (int) HttpStatusCode.Forbidden;
            }
            else
            {
                var dotaGames = _context.DotaGames.Include(g => g.GamePlayers).ThenInclude(dgp => dgp.Player)
                    .AsQueryable();
                if (gameClientId != null)
                {
                    dotaGames = dotaGames.Where(g => g.GameClientId == gameClientId);
                }

                foreach (var dotaGame in dotaGames)
                {
                    retVal.Add(DotaGameDto.FromModel(dotaGame));
                }
            }

            return retVal.AsEnumerable();
        }

        [HttpGet("{id}")]
        public DotaGameDto Get(ulong id)
        {
            var retVal = default(DotaGameDto);

            Request.Headers.TryGetValue(HttpRequestHeader.Authorization.ToString(), out var appKey);
            if (appKey.Count == 0 || appKey[0] != AppKey)
            {
                Response.StatusCode = (int) HttpStatusCode.Forbidden;
            }
            else
            {
                var dotaGames = _context.DotaGames.Where(g => g.Id == id).Include(g => g.GamePlayers)
                    .ThenInclude(dgp => dgp.Player);
                if (dotaGames.Any())
                    retVal = DotaGameDto.FromModel(dotaGames.First());
                else
                    Response.StatusCode = (int) HttpStatusCode.NotFound;
            }

            return retVal;
        }

        [HttpPost]
        public DotaGameDto Post([FromBody] DotaGameDto gameDto)
        {
            var retVal = default(DotaGameDto);

            Request.Headers.TryGetValue(HttpRequestHeader.Authorization.ToString(), out var appKey);
            if (appKey.Count == 0 || appKey[0] != AppKey)
            {
                Response.StatusCode = (int) HttpStatusCode.Forbidden;
            }
            else
            {
                var gameModel = new DotaGame
                {
                    GameClientId = gameDto.GameClientId,
                    ResultAsString = gameDto.Result.ToString(),
                    GamePlayers = new List<DotaGamePlayer>()
                };

                _context.Add(gameModel);
                _context.SaveChanges();

                AddPlayersToGame(_context, gameModel, gameDto);

                retVal = DotaGameDto.FromModel(gameModel);
            }

            return retVal;
        }

        private static void AddPlayersToGame(ApplicationDbContext context, DotaGame gameModel, DotaGameDto dotaGameDto)
        {
            foreach (var gamePlayerDto in dotaGameDto.DotaGamePlayers)
            {
                var playerDto = gamePlayerDto.Player;

                var playerModel =
                    context.Players.FirstOrDefault(pl => pl.PlayerClientId == playerDto.PlayerClientId);
                if (playerModel == default(Player))
                {
                    playerModel = new Player
                    {
                        PlayerClientId = playerDto.PlayerClientId,
                        Nickname = playerDto.Nickname
                    };
                    context.Add(playerModel);
                }

                var gamePlayerModel = new DotaGamePlayer
                {
                    Player = playerModel,
                    Game = gameModel,
                    IsCaptain = gamePlayerDto.IsCaptain,
                    PickOrder = gamePlayerDto.PickOrder
                };

                switch (gamePlayerDto.TeamType)
                {
                    case DotaGamePlayerDto.DotaTeamType.Dire:
                        gamePlayerModel.TeamType = DotaGamePlayer.DotaTeamType.Dire;
                        break;
                    case DotaGamePlayerDto.DotaTeamType.Radiant:
                        gamePlayerModel.TeamType = DotaGamePlayer.DotaTeamType.Radiant;
                        break;
                    default: throw new NotImplementedException();
                }

                gameModel.GamePlayers.Add(gamePlayerModel);
                context.SaveChanges();
            }
        }
    }
}