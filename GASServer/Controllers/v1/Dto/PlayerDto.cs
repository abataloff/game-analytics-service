﻿using GASServer.Models;
using Newtonsoft.Json;

namespace GASServer.Controllers.v1.Dto
{
    public class PlayerDto
    {
        [JsonProperty(PropertyName = "playerClientId")]
        public string PlayerClientId { get; set; }

        [JsonProperty(PropertyName = "nickName")]
        public string Nickname { get; set; }

        public static PlayerDto FromModel(Player player)
        {
            return new PlayerDto
            {
                PlayerClientId = player.PlayerClientId,
                Nickname = player.Nickname
            };
        }
    }
}