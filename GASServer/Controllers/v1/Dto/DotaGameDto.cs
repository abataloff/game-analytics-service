﻿using System;
using System.Collections.Generic;
using GASServer.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GASServer.Controllers.v1.Dto
{
    public class DotaGameDto
    {
        public enum DotaGameResult
        {
            Undefined = 0,
            DireWinner = 1,
            RadiantWinner = 2,
            Draw = 3
        }

        /// <summary>
        /// ID игры в GAS.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public ulong Id { get; set; }

        /// <summary>
        /// ID игры у клиента.
        /// </summary>
        [JsonProperty(PropertyName = "gameClientId")]
        public string GameClientId { get; set; }

        /// <summary>
        /// Список учасников игры.
        /// </summary>
        [JsonProperty(PropertyName = "players")]
        public DotaGamePlayerDto[] DotaGamePlayers { get; set; }

        /// <summary>
        /// Исход игры. Ограничений варианты значений нет.
        /// </summary>
        [JsonProperty(PropertyName = "result"), JsonConverter(typeof(StringEnumConverter))]
        public DotaGameResult Result { get; set; }

        public static DotaGameDto FromModel(DotaGame dotaGame)
        {
            var retVal = new DotaGameDto
            {
                Id = dotaGame.Id,
                GameClientId = dotaGame.GameClientId
            };

            switch (dotaGame.Result)
            {
                case DotaGame.DotaGameResult.DireWinner:
                    retVal.Result = DotaGameResult.DireWinner;
                    break;

                case DotaGame.DotaGameResult.RadiantWinner:
                    retVal.Result = DotaGameResult.RadiantWinner;
                    break;

                case DotaGame.DotaGameResult.Draw:
                    retVal.Result = DotaGameResult.Draw;
                    break;

                case DotaGame.DotaGameResult.Undefined:
                    retVal.Result = DotaGameResult.Undefined;
                    break;
                default: throw new NotImplementedException();
            }

            var player = new List<DotaGamePlayerDto>();
            foreach (var gamePlayer in dotaGame.GamePlayers)
            {
                player.Add(DotaGamePlayerDto.FromModel(gamePlayer));
            }

            retVal.DotaGamePlayers = player.ToArray();

            return retVal;
        }
    }
}