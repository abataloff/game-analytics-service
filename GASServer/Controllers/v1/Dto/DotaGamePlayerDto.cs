﻿using System;
using GASServer.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GASServer.Controllers.v1.Dto
{
    public class DotaGamePlayerDto
    {
        public enum DotaTeamType
        {
            Radiant = 0,
            Dire = 1
        }

        [JsonProperty(PropertyName = "isCaptain")]
        public bool IsCaptain { get; set; }

        [JsonProperty(PropertyName = "teamType"), JsonConverter(typeof(StringEnumConverter))]
        public DotaTeamType TeamType { get; set; }

        [JsonProperty(PropertyName = "pickOrder")]
        public uint PickOrder { get; set; }

        [JsonProperty(PropertyName = "player")]
        public PlayerDto Player { get; set; }

        public static DotaGamePlayerDto FromModel(DotaGamePlayer dotaGamePlayer)
        {
            var retVal = new DotaGamePlayerDto
            {
                IsCaptain = dotaGamePlayer.IsCaptain,
                PickOrder = dotaGamePlayer.PickOrder,
                Player = PlayerDto.FromModel(dotaGamePlayer.Player)
            };

            switch (dotaGamePlayer.TeamType)
            {
                case DotaGamePlayer.DotaTeamType.Dire:
                    retVal.TeamType = DotaTeamType.Dire;
                    break;

                case DotaGamePlayer.DotaTeamType.Radiant:
                    retVal.TeamType = DotaTeamType.Radiant;
                    break;
                default: throw new NotImplementedException();
            }

            return retVal;
        }
    }
}