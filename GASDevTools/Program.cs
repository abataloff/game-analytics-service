﻿using System;
using System.Collections.Generic;
using System.Linq;
using v0 = GASClient.v0.Dto;
using v1 = GASClient.v1.Dto;

namespace GASDevTools
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Начало");

            try
            {
                var localhost = "http://localhost:5000/";
                var prodhost = "http://gas.abataloff.ru";
                var currenthost = localhost;
                var clientV0 = new GASClient.v0.Client(currenthost, "UIR-FS8-KJE");
                var clientV1 = new GASClient.v1.Client(currenthost, "UIR-FS8-KJE");

                {
                    var gameClientId = GetRundomString();
                    var dotaGameV0 = new v0.DotaGameDto
                    {
                        GameClientId = gameClientId,
                        DireTeam = "dir1V0,dir2V0,dir3V0,dir4V0,dir5V0",
                        RadiantTeam = "rad1V0,rad2V0,rad3V0,rad4V0,rad5V0",
                        Result = v1.DotaGameDto.DotaGameResult.DireWinner.ToString()
                    };

                    var dotaGameV0AddDotaGameResult = clientV0.AddDotaGameResult(dotaGameV0).DotaGame;
                    Console.WriteLine("Сравнение dotaGameV0 и dotaGameV0AddDotaGameResult");
                    Compare(dotaGameV0, dotaGameV0AddDotaGameResult);

                    var dotaGameV0GetByGameClientId =
                        clientV0.GetDotaGameResultsByGameClientId(gameClientId).DotaGames[0];
                    Console.WriteLine("Сравнение dotaGameV0AddDotaGameResult и dotaGameV0GetByGameClientId");
                    Compare(dotaGameV0AddDotaGameResult, dotaGameV0GetByGameClientId, true);

                    var dotaGameV0GetDotaGameResult =
                        clientV0.GetDotaGameResult(dotaGameV0AddDotaGameResult.Id).DotaGame;
                    Console.WriteLine("Сравнение dotaGameV0AddDotaGameResult и dotaGameV0GetDotaGameResult");
                    Compare(dotaGameV0AddDotaGameResult, dotaGameV0GetDotaGameResult, true);

                    var dotaGameV1GetByGameClientId =
                        clientV1.GetDotaGameResultsByGameClientId(gameClientId).DotaGames[0];
                    Console.WriteLine("Сравнение dotaGameV0AddDotaGameResult и dotaGameV1GetByGameClientId");
                    Compare(dotaGameV0GetDotaGameResult, dotaGameV1GetByGameClientId, true);

                    var dotaGameV1GetDotaGameResult =
                        clientV1.GetDotaGameResult(dotaGameV0AddDotaGameResult.Id).DotaGame;
                    Console.WriteLine("Сравнение dotaGameV0AddDotaGameResult и dotaGameV1GetDotaGameResult");
                    Compare(dotaGameV0AddDotaGameResult, dotaGameV1GetDotaGameResult, true);
                }
                {
                    var gameClientId = GetRundomString();
                    var dotaGameV1 = GenerateDotaGameV1(gameClientId);

                    var dotaGameV1AddDotaGameResult = clientV1.AddDotaGameResult(dotaGameV1).DotaGame;
                    Console.WriteLine("Сравнение dotaGameV1 и dotaGameV1AddDotaGameResult");
                    Compare(dotaGameV1, dotaGameV1AddDotaGameResult);

                    var dotaGameV1GetByGameClientId =
                        clientV1.GetDotaGameResultsByGameClientId(gameClientId).DotaGames[0];
                    Console.WriteLine("Сравнение dotaGameV1AddDotaGameResult и dotaGameV1GetByGameClientId");
                    Compare(dotaGameV1AddDotaGameResult, dotaGameV1GetByGameClientId, true);

                    var dotaGameV1GetDotaGameResult =
                        clientV1.GetDotaGameResult(dotaGameV1AddDotaGameResult.Id).DotaGame;
                    Console.WriteLine("Сравнение dotaGameV1AddDotaGameResult и dotaGameV1GetDotaGameResult");
                    Compare(dotaGameV1AddDotaGameResult, dotaGameV1GetDotaGameResult, true);

                    var dotaGameV0GetByGameClientId =
                        clientV0.GetDotaGameResultsByGameClientId(gameClientId).DotaGames[0];
                    Console.WriteLine("Сравнение dotaGameV1AddDotaGameResult и dotaGameV0GetByGameClientId");
                    Compare(dotaGameV0GetByGameClientId, dotaGameV1GetDotaGameResult, true);

                    var dotaGameV0GetDotaGameResult =
                        clientV0.GetDotaGameResult(dotaGameV1AddDotaGameResult.Id).DotaGame;
                    Console.WriteLine("Сравнение dotaGameV1AddDotaGameResult и dotaGameV1GetDotaGameResult");
                    Compare(dotaGameV0GetDotaGameResult, dotaGameV1AddDotaGameResult, true);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Ошибка {0}", exception);
            }

            Console.WriteLine("Конец");
        }

        private static v1.DotaGameDto GenerateDotaGameV1(string gameClientId)
        {
            var retVal = new v1.DotaGameDto
            {
                GameClientId = gameClientId,
                Result = v1.DotaGameDto.DotaGameResult.RadiantWinner,
            };

            var dotaGamePlayer = new List<v1.DotaGamePlayerDto>();
            for (var i = 1; i <= 5; i++)
            {
                dotaGamePlayer.Add(new v1.DotaGamePlayerDto
                {
                    Player = new v1.PlayerDto
                    {
                        PlayerClientId = string.Format("dir{0}V1", i),
                        Nickname = string.Format("dir{0}V1_nick", i)
                    },
                    IsCaptain = i == 1,
                    PickOrder = (uint) i,
                    TeamType = v1.DotaGamePlayerDto.DotaTeamType.Dire
                });
            }

            for (var i = 1; i <= 5; i++)
            {
                dotaGamePlayer.Add(new v1.DotaGamePlayerDto
                {
                    Player = new v1.PlayerDto
                    {
                        PlayerClientId = string.Format("rad{0}V1", i),
                        Nickname = string.Format("rad{0}V1_nick", i)
                    },
                    IsCaptain = i == 1,
                    PickOrder = (uint) i,
                    TeamType = v1.DotaGamePlayerDto.DotaTeamType.Radiant
                });
            }

            retVal.DotaGamePlayers = dotaGamePlayer.ToArray();

            return retVal;
        }

        private static void Compare(v0.DotaGameDto a, v0.DotaGameDto b, bool includeId = false)
        {
            var isError = false;

            if (includeId)
            {
                if (a.Id != b.Id)
                {
                    Console.WriteLine("Не равны DotaGame.Id {0}!={1}", a.Id, b.Id);
                    isError = true;
                }
            }

            if (a.GameClientId != b.GameClientId)
            {
                Console.WriteLine("Не равны DotaGame.GameClientId {0}!={1}", a.GameClientId, b.GameClientId);
                isError = true;
            }

            if (a.RadiantTeam != b.RadiantTeam)
            {
                Console.WriteLine("Не равны DotaGame.RadiantTeam {0}!={1}", a.RadiantTeam, b.RadiantTeam);
                isError = true;
            }

            if (a.DireTeam != b.DireTeam)
            {
                Console.WriteLine("Не равны DotaGame.DireTeam {0}!={1}", a.DireTeam, b.DireTeam);
                isError = true;
            }

            if (a.Result != b.Result)
            {
                Console.WriteLine("Не равны DotaGame.Result {0}!={1}", a.Result, b.Result);
                isError = true;
            }

            if (!isError)
            {
                Console.WriteLine("Совпадают");
            }
        }

        private static void Compare(v1.DotaGameDto a, v1.DotaGameDto b, bool includeId = false)
        {
            var isError = false;

            if (includeId)
            {
                if (a.Id != b.Id)
                {
                    Console.WriteLine("Не равны DotaGame.Id {0}!={1}", a.Id, b.Id);
                    isError = true;
                }
            }

            if (a.GameClientId != b.GameClientId)
            {
                Console.WriteLine("Не равны DotaGame.GameClientId {0}!={1}", a.GameClientId, b.GameClientId);
                isError = true;
            }

            if (a.Result != b.Result)
            {
                Console.WriteLine("Не равны DotaGame.Result {0}!={1}", a.Result, b.Result);
                isError = true;
            }

            var aPlayers = new Dictionary<string, v1.DotaGamePlayerDto>();
            foreach (var dotaGamePlayer in a.DotaGamePlayers)
            {
                aPlayers.Add(dotaGamePlayer.Player.PlayerClientId, dotaGamePlayer);
            }

            var bPlayers = new Dictionary<string, v1.DotaGamePlayerDto>();
            foreach (var dotaGamePlayer in b.DotaGamePlayers)
            {
                var playerClientId = dotaGamePlayer.Player.PlayerClientId;
                bPlayers.Add(playerClientId, dotaGamePlayer);
                isError = !Compare(aPlayers[playerClientId], dotaGamePlayer);
            }

            foreach (var aPlayersKey in aPlayers.Keys)
            {
                isError = !Compare(aPlayers[aPlayersKey], bPlayers[aPlayersKey]);
            }

            if (!isError)
            {
                Console.WriteLine("Совпадают");
            }
        }

        private static bool Compare(v1.DotaGamePlayerDto a, v1.DotaGamePlayerDto b)
        {
            var retVal = true;

            if (a.IsCaptain != b.IsCaptain)
            {
                Console.WriteLine("Не соответствуют капитаны");
                retVal = false;
            }

            if (a.PickOrder != b.PickOrder)
            {
                Console.WriteLine("Не соответствуют PickOrder");
                retVal = false;
            }

            if (a.TeamType != b.TeamType)
            {
                Console.WriteLine("Не соответствуют TeamType");
                retVal = false;
            }

            retVal &= Compare(a.Player, b.Player);

            return retVal;
        }

        private static bool Compare(v1.PlayerDto a, v1.PlayerDto b)
        {
            var retVal = true;

            if (a.Nickname != b.Nickname)
            {
                Console.WriteLine("Не соответствуют Nickname");
                retVal = false;
            }


            if (a.PlayerClientId != b.PlayerClientId)
            {
                Console.WriteLine("Не соответствуют PlayerClientId");
                retVal = false;
            }

            return retVal;
        }

        private static void Compare(v0.DotaGameDto v0, v1.DotaGameDto v1, bool includeId = false)
        {
            var isError = false;

            if (includeId)
            {
                if (v0.Id != v1.Id)
                {
                    Console.WriteLine("Не равны DotaGame.Id {0}!={1}", v0.Id, v1.Id);
                    isError = true;
                }
            }

            if (v0.GameClientId != v1.GameClientId)
            {
                Console.WriteLine("Не равны GameClientId {0}!={1}", v0.GameClientId, v1.GameClientId);
                isError = true;
            }

            if (v0.Result != v1.Result.ToString())
            {
                Console.WriteLine("Не равны Result {0}!={1}", v0.Result, v1.Result);
                isError = true;
            }

            var direPlayerClientIdsV0 = v0.DireTeam.Split(",");
            var radiantPlayerClientIdsV0 = v0.RadiantTeam.Split(",");
            var dotaGamePlayersV1 = v1.DotaGamePlayers;
            isError &= CompareTeam(dotaGamePlayersV1, direPlayerClientIdsV0, radiantPlayerClientIdsV0);

            if (!isError)
            {
                Console.WriteLine("Совпадают");
            }
        }

        private static bool CompareTeam(v1.DotaGamePlayerDto[] dotaGamePlayersV1, string[] direPlayerClientIdsV0,
            string[] radiantPlayerClientIdsV0)
        {
            var isError = false;

            if (dotaGamePlayersV1 == null)
            {
                Console.WriteLine("Пустое значение (null) v1.DotaGamePlayers");
                isError = true;
            }
            else if (direPlayerClientIdsV0.Length + radiantPlayerClientIdsV0.Length != dotaGamePlayersV1.Length)
            {
                Console.WriteLine("Не равное количество игроков");
                isError = true;
            }
            else
            {
                var direCaptainIdV1 = default(string);
                var radiantCaptainIdV1 = default(string);
                var direPlayerClientIdsV1 = new List<string>();
                var radiantPlayerClientIdsV1 = new List<string>();
                foreach (var dotaGamePlayer in dotaGamePlayersV1)
                {
                    var isCaptain = dotaGamePlayer.IsCaptain;
                    var playerClientId = dotaGamePlayer.Player.PlayerClientId;
                    string[] ids;
                    switch (dotaGamePlayer.TeamType)
                    {
                        case v1.DotaGamePlayerDto.DotaTeamType.Dire:
                            ids = direPlayerClientIdsV0;
                            if (isCaptain)
                            {
                                direCaptainIdV1 = playerClientId;
                            }

                            direPlayerClientIdsV1.Add(playerClientId);
                            break;
                        case v1.DotaGamePlayerDto.DotaTeamType.Radiant:
                            ids = radiantPlayerClientIdsV0;
                            if (isCaptain)
                            {
                                radiantCaptainIdV1 = playerClientId;
                            }

                            radiantPlayerClientIdsV1.Add(playerClientId);
                            break;
                        default:
                            throw new NotImplementedException();
                    }

                    if (!ids.Contains(playerClientId))
                    {
                        Console.WriteLine("Не найден Id игрока ({0}) из V1 в V0", playerClientId);
                        isError = true;
                    }
                }

                if (direCaptainIdV1 != direPlayerClientIdsV0[0])
                {
                    Console.WriteLine("Не соответствуют капитаны команды Dire");
                    isError = true;
                }

                if (radiantCaptainIdV1 != radiantPlayerClientIdsV0[0])
                {
                    Console.WriteLine("Не соответствуют капитаны команды Radiant");
                    isError = true;
                }

                foreach (var direPlayerClientIdV0 in direPlayerClientIdsV0)
                {
                    if (!direPlayerClientIdsV1.Contains(direPlayerClientIdV0))
                    {
                        Console.WriteLine("Не найден Id игрока ({0}) из V0 в V1", direPlayerClientIdV0);
                        isError = true;
                    }
                }

                foreach (var radiantPlayerClientIdV0 in radiantPlayerClientIdsV0)
                {
                    if (!radiantPlayerClientIdsV1.Contains(radiantPlayerClientIdV0))
                    {
                        Console.WriteLine("Не найден Id игрока ({0}) из V0 в V1", radiantPlayerClientIdV0);
                        isError = true;
                    }
                }
            }

            return isError;
        }

        private static string GetRundomString()
        {
            string retVal;

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            retVal = new string(Enumerable.Repeat(chars, 8)
                .Select(s => s[random.Next(s.Length)]).ToArray());

            return retVal;
        }
    }
}