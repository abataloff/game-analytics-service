#!/usr/bin/env bash

rm -f -- $(pwd)/sqlite.db
touch $(pwd)/sqlite.db
docker run --rm -v $(pwd)/migrations:/root/migrations -v $(pwd)/sqlite.db:/root/sqlite.db abataloff/dbmigrator