-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE sqlitebrowser_rename_column_new_table
(
  id              INTEGER PRIMARY KEY NOT NULL,
  game_client_id  TEXT NOT NULL,
  result          TEXT NOT NULL
);
INSERT INTO sqlitebrowser_rename_column_new_table SELECT id,game_client_id,result FROM DotaGames;
DROP TABLE DotaGames;
ALTER TABLE sqlitebrowser_rename_column_new_table RENAME TO DotaGames;
CREATE UNIQUE INDEX DotaGames_id_uindex
  ON DotaGames (id);
-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
ALTER TABLE DotaGames ADD dire_team TEXT NOT NULL;
ALTER TABLE DotaGames ADD radiant_team TEXT NOT NULL;