-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE Players
(
  id                INTEGER PRIMARY KEY NOT NULL,
  player_client_id  TEXT NOT NULL,
  nickname          TEXT NOT NULL
);
CREATE UNIQUE INDEX Players_id_uindex
  ON Players (id);

CREATE TABLE DotaGamePlayers
(
  dota_game_id      INTEGER NOT NULL,
  player_id         INTEGER NOT NULL,
  is_capitan        INTEGER NOT NULL,
  team_type         INTEGER NOT NULL,
  pick_order        INTEGER NOT NULL,
  PRIMARY KEY (dota_game_id, player_id)
);
CREATE UNIQUE INDEX DotaGamePlayers_id_uindex
  ON DotaGamePlayers (dota_game_id, player_id);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE Players;
DROP INDEX Players_id_uindex;
DROP TABLE DotaGamePlayers;
DROP INDEX DotaGamePlayers_id_uindex;