-- +migrate Up
-- SQL in section 'Up' is executed when this migration is applied
CREATE TABLE DotaGames
(
  id              INTEGER PRIMARY KEY NOT NULL,
  game_client_id  TEXT NOT NULL,
  dire_team       TEXT NOT NULL,
  radiant_team    TEXT NOT NULL,
  result          TEXT NOT NULL
);
CREATE UNIQUE INDEX DotaGames_id_uindex
  ON DotaGames (id);

-- +migrate Down
-- SQL section 'Down' is executed when this migration is rolled back
DROP TABLE DotaGames;
DROP INDEX DotaGames_id_uindex;