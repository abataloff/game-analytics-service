#!/usr/bin/env bash

docker run --rm -v $(pwd)/migrations:/root/migrations -v $(pwd)/sqlite.db:/root/sqlite.db abataloff/dbmigrator