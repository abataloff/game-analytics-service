﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GASClient.v1.Dto
{
    public class DotaGameDto
    {
        public enum DotaGameResult
        {
            Undefined = 0,
            DireWinner = 1,
            RadiantWinner = 2,
            Draw = 3
        }

        /// <summary>
        /// ID игры в GAS.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public ulong Id { get; set; }

        /// <summary>
        /// ID игры у клиента.
        /// </summary>
        [JsonProperty(PropertyName = "gameClientId")]
        public string GameClientId { get; set; }

        /// <summary>
        /// Список учасников игры.
        /// </summary>
        [JsonProperty(PropertyName = "players")]
        public DotaGamePlayerDto[] DotaGamePlayers { get; set; }

        /// <summary>
        /// Исход игры. Ограничений варианты значений нет.
        /// </summary>
        [JsonProperty(PropertyName = "result"), JsonConverter(typeof(StringEnumConverter))]
        public DotaGameResult Result { get; set; }
    }
}