﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace GASClient.v1.Dto
{
    public class DotaGamePlayerDto
    {
        public enum DotaTeamType
        {
            Radiant = 0,
            Dire = 1
        }

        [JsonProperty(PropertyName = "isCaptain")]
        public bool IsCaptain { get; set; }

        [JsonProperty(PropertyName = "teamType"), JsonConverter(typeof(StringEnumConverter))]
        public DotaTeamType TeamType { get; set; }

        [JsonProperty(PropertyName = "pickOrder")]
        public uint PickOrder { get; set; }

        [JsonProperty(PropertyName = "player")]
        public PlayerDto Player { get; set; }
    }
}