﻿using Newtonsoft.Json;

namespace GASClient.v1.Dto
{
    public class PlayerDto
    {
        [JsonProperty(PropertyName = "playerClientId")]
        public string PlayerClientId { get; set; }

        [JsonProperty(PropertyName = "nickName")]
        public string Nickname { get; set; }
    }
}