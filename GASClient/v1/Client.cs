﻿using System.Net;
using GASClient.v1.Dto;

namespace GASClient.v1
{
    public class Client : ClientBase
    {
        public Client(string host, string appKey) : base("v1", host, appKey)
        {
        }

        /// <summary>
        /// Добавляет результат игры Dota.
        /// </summary>
        /// <param name="dotaGame">Игра.</param>
        /// <returns>Если HttpStatusCode - 200 OK, то возвращает DotaGame, иначае в HttpStatusCode код ошибки.</returns>
        public (DotaGameDto DotaGame, HttpStatusCode HttpStatusCode) AddDotaGameResult(DotaGameDto dotaGame)
        {
            return base.AddDotaGameResult(dotaGame);
        }

        /// <summary>
        /// Добавляет результаты нескольких игр Dota.
        /// </summary>
        /// <param name="dotaGames">Массив игр.</param>
        /// <returns>Если HttpStatusCode - 200 OK, то возвращает массив DotaGame в той же последовательности что и переданы результаты, иначае в HttpStatusCode код ошибки.</returns>
        public (DotaGameDto[] DotaGame, HttpStatusCode HttpStatusCode) AddDotaGameResults(DotaGameDto[] dotaGames)
        {
            return base.AddDotaGameResults(dotaGames);
        }

        /// <summary>
        /// Получает результат игры.
        /// </summary>
        /// <param name="gameId">ID игры в GAS.</param>
        /// <returns>Если HttpStatusCode - 200 OK, то возвращает DotaGame, иначае в HttpStatusCode код ошибки.</returns>
        public (DotaGameDto DotaGame, HttpStatusCode HttpStatusCode) GetDotaGameResult(ulong gameId)
        {
            return base.GetDotaGameResult<DotaGameDto>(gameId);
        }

        /// <summary>
        /// Получает результат игр по Id игры у клиента.
        /// </summary>
        /// <param name="gameClientId">ID игры у клиента.</param>
        /// <returns>Если HttpStatusCode - 200 OK, то возвращает DotaGames, иначае в HttpStatusCode код ошибки.</returns>
        public (DotaGameDto[] DotaGames, HttpStatusCode HttpStatusCode) GetDotaGameResultsByGameClientId(
            string gameClientId)
        {
            return base.GetDotaGameResultsByGameClientId<DotaGameDto>(gameClientId);
        }
    }
}