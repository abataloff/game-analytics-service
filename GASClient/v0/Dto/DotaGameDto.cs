﻿using Newtonsoft.Json;

namespace GASClient.v0.Dto
{
    public class DotaGameDto
    {
        /// <summary>
        /// ID игры в GAS.
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public ulong Id { get; set; }

        /// <summary>
        /// ID игры у клиента.
        /// </summary>
        [JsonProperty(PropertyName = "gameClientId")]
        public string GameClientId { get; set; }

        /// <summary>
        /// Список ID участников команды тьмы (разделитель - запятая). Самый первый в списке идет капитан.
        /// </summary>
        [JsonProperty(PropertyName = "direTeam")]
        public string DireTeam { get; set; }

        /// <summary>
        /// Список ID участников команды света (разделитель - запятая). Самый первый в списке идет капитан.
        /// </summary>
        [JsonProperty(PropertyName = "radiantTeam")]
        public string RadiantTeam { get; set; }

        /// <summary>
        /// Исход игры. Ограничений варианты значений нет.
        /// </summary>
        [JsonProperty(PropertyName = "result")]
        public string Result { get; set; }
    }
}