﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace GASClient
{
    public abstract class ClientBase
    {
        private readonly Uri _dotaGamesUri;
        private readonly string _appKey;


        protected ClientBase(string version, string host, string appKey)
        {
            var rootUri = new Uri(new Uri(host), version).AbsoluteUri;
            _dotaGamesUri = new Uri(rootUri + "/dotagames");
            _appKey = appKey;
        }

        protected (T dotaGame, HttpStatusCode httpStatusCode) AddDotaGameResult<T>(T dotaGame)
        {
            var request = WebRequest.CreateHttp(_dotaGamesUri);
            request.Headers.Add(HttpRequestHeader.Authorization, _appKey);
            request.MediaType = "application/json";
            request.ContentType = "application/json";
            request.Method = WebRequestMethods.Http.Post;
            var requestStream = request.GetRequestStream();
            TextWriter requestBody = new StreamWriter(requestStream);
            requestBody.Write(JsonConvert.SerializeObject(dotaGame));
            requestBody.Close();
            requestStream.Close();

            var response = (HttpWebResponse) request.GetResponse();

            var retHttpStatusCode = response.StatusCode;

            T retDotaGame = default(T);

            if (retHttpStatusCode == HttpStatusCode.OK)
            {
                var sr = new StreamReader(response.GetResponseStream());
                var responseBody = sr.ReadToEnd();
                retDotaGame = JsonConvert.DeserializeObject<T>(responseBody);
            }

            return (retDotaGame, retHttpStatusCode);
        }

        protected (T[] dotaGames, HttpStatusCode httpStatusCode) AddDotaGameResults<T>(IEnumerable<T> dotaGames)
        {
            var retList = new List<T>();
            var retHttpStatusCode = HttpStatusCode.OK;

            foreach (var dataGame in dotaGames)
            {
                var result = AddDotaGameResult(dataGame);
                if (result.httpStatusCode != HttpStatusCode.OK)
                {
                    retHttpStatusCode = result.httpStatusCode;
                    continue;
                }

                retList.Add(result.dotaGame);
            }

            return (retList.ToArray(), retHttpStatusCode);
        }

        protected (T dotaGame, HttpStatusCode httpStatusCode) GetDotaGameResult<T>(ulong gameId)
        {
            var request = WebRequest.CreateHttp(_dotaGamesUri + "/" + gameId);
            request.Method = WebRequestMethods.Http.Get;
            request.MediaType = "application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, _appKey);
            var response = (HttpWebResponse) request.GetResponse();

            var retDotaGame = default(T);
            var retHttpStatusCode = response.StatusCode;

            if (retHttpStatusCode == HttpStatusCode.OK)
            {
                var sr = new StreamReader(response.GetResponseStream());
                var body = sr.ReadToEnd();
                retDotaGame = JsonConvert.DeserializeObject<T>(body);
            }

            return (retDotaGame, retHttpStatusCode);
        }

        protected (T[] dotaGame, HttpStatusCode httpStatusCode) GetDotaGameResultsByGameClientId<T>(
            string gameClientId)
        {
            var request =
                WebRequest.CreateHttp(_dotaGamesUri + string.Format("?gameClientId={0}", gameClientId));
            request.Method = WebRequestMethods.Http.Get;
            request.MediaType = "application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, _appKey);
            var response = (HttpWebResponse) request.GetResponse();

            T[] retDotaGame = null;
            var retHttpStatusCode = response.StatusCode;

            if (retHttpStatusCode == HttpStatusCode.OK)
            {
                var sr = new StreamReader(response.GetResponseStream());
                var body = sr.ReadToEnd();
                retDotaGame = JsonConvert.DeserializeObject<T[]>(body);
            }

            return (retDotaGame, retHttpStatusCode);
        }
    }
}